#include "Arduino.h"
#include "GyroControl.h"


GyroControl::GyroControl( 
    const char* wifi_ssid,
    const char* wifi_password )
    : server( 80 ), 
    local_ip( 192,168,101,1 ),
    gateway( 192,168,101,1 ),
    subnet( 255,255,255,0 )
{
    ssid = wifi_ssid;
    password = wifi_password;
}


void GyroControl::init()
{
    WiFi.softAP( ssid, password );
    WiFi.softAPConfig( local_ip, gateway, subnet );
    delay(100);

    server.begin();
}


void GyroControl::onConnect( GyroConfig& settings )
{
    WiFiClient client = server.available();

    if ( client )
    {
        String request = client.readStringUntil('\r');
        
        String key = "";
        String value;

        int strLen = request.length()+1;
        char req[strLen];
        request.toCharArray(req, strLen);
        
        for ( String temp = strtok(req, " "); temp; temp = strtok(NULL, " ") ) {
            if (temp.startsWith("/?")) 
            {
                int equalIdx = temp.indexOf("=");
                key = temp.substring(2, equalIdx);
                value = temp.substring(equalIdx+1);

                break;
            }
        }

        // handle GET parameter and save parameter to config
        if ( key == "rpm" )
        {
            settings.setRPS( value.toFloat() );
        }
        else if ( key == "angle" )
        {
            settings.addAngle( value.toFloat() );
        }
        else if ( key == "active" )
        {
            bool val = value.toInt() == 1 ? true : false;
            settings.setActive( val );
            settings.setAngle( 0 );
        }

        request="";

        String currentLine = "";
        String header;
        while ( client.connected() ) {
            if ( client.available() ) {
                char c = client.read();             // read available byte

                header += c;
                if ( c == '\n' ) {
                    // if the current line is blank, you got two newline characters in a row.
                    // that's the end of the client HTTP request, so send a response
                    if ( currentLine.length() == 0 ) {
                        client.println("HTTP/1.1 200 OK");
                        client.println("Content-type:text/html");
                        client.println("Connection: close");
                        client.println();
                        
                        // The HTTP response ends with another blank line
                        client.println();

                        break;
                    } else {
                        // on a newline, clear currentLine
                        currentLine = "";
                    }
                } else if ( c != '\r' ) {
                    currentLine += c;      // on NO carriage return concat incoming bytes
                }
            }
        }

    header = "";
    
    String html = sendHTML( 
        settings.getRPS(), 
        settings.getAngle(), 
        settings.getActive() );
    client.print( html );

    client.stop();
  }
}


String GyroControl::sendHTML( 
    float rps,
    float angle,
    bool active )
{
    String activeColor = ( active ? "rgba(244, 67, 54, 0.8)" : "rgb(33, 150, 243, .6)" );
    String ptr = "<!DOCTYPE html>"
        "<html lang=\"en\">"
            "<head>"
                "<title>Gyro Control</title>"
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
                "<meta charset=\"UTF-8\">"

                "<style>"
                    "html {"
                        "font-family: Helvetica;"
                        "display:inline-block;"
                        "margin: 0px auto;"
                        "text-align: center;"
                    "}"
                    "h1 {"
                        "margin-top: 0;"
                        "font-size: 5em;"
                    "}"
                    "p {"
                        "font-size: 3rem;"
                        "padding: 0;"
                        "margin: 0;"
                    "}"
                    "#rpm, #angle, #angle-button {"
                        "vertical-align: middle;"
                    "}"
                    ".button {"
                        "vertical-align: middle;"
                        "display: inline-block;"
                        "background-color: rgba(0,0,0,.5);"
                        "border: none;"
                        "border-radius: 4px;"
                        "color: white;"
                        "text-decoration: none;"
                        "cursor: pointer;"
                        "padding: 2px 5px;"
                        "font-size: 24px;"
                    "}"
                    ".divTable {"
                        "display: table;"
                        "width: 100%;"
                    "}"
                    ".divTableRow {"
                        "display: table-row;"
                        "margin-top: 20px;"
                    "}"
                    ".divTableCell {"
                        "display: table-cell;"
                        "padding: 3px 10px;"
                    "}"
                    ".divTableBody {"
                        "display: table-row-group;"
                    "}"
                    "#active_button {"
                        "width: 70px;"
                        "height: 70px;"
                        "border-radius: 35px;"
                        "border: 1px solid rgba(0,0,0,.5);"
                        "font-size: 50px;"
                        "background-color: unset;"
                    "}"
                    "#rpm { width: 300px; }"
                    ".gauge { vertical-align:top; }"
                    "#active-button { margin-bottom: 1em; }"
                "</style>"

                "<script>"
                    "function ajax(key) {"
                        "var value = document.getElementById(key).value;\n"
                        "var queryStr = key + '=' + value;\n"

                        "var xhr = new XMLHttpRequest();\n"
                        "xhr.open('GET', '/?'+queryStr);\n"
                        "xhr.onerror = function () {\n"
                            "document.getElementById(key + '_value').style.color = 'rgb(244, 67, 54, 0.8)';\n"
                        "}\n"
                        "xhr.onload = function() {\n"
                            "if (xhr.status === 200) {\n"
                                "if (key !== 'active') {\n"
                                    "document.getElementById(key + '_value').style.color = 'rgb(0, 0, 0, 1)';\n"
                                    "document.getElementById(key+'_value').textContent = value;\n"
                                "} else {\n"
                                    "document.getElementById(key).value = value == 1 ? 0 : 1;\n"
                                    "document.getElementById(key+'_button').style.backgroundColor = value == 1 ? 'rgba(244, 67, 54, 0.8)' : 'rgb(33, 150, 243, .6)';\n"
                                "}\n"
                            "}\n"
                        "}\n"
                        "xhr.send();\n"
                    "}\n"
                    "function showValue(key) {\n"
                        "var value = document.getElementById(key).value;\n"
                        "document.getElementById(key + '_value').textContent = value;\n"
                    "}\n"
                "</script>"
            "</head>"

            "<body>"
                "<div style=\"margin-top:50px;\">"
                    "<svg width=\"300\" height=\"95\" viewBox=\"0 0 430.18205 135.46667\"><g id=\"layer1\" transform=\"translate(294.98011)\"><path style=\"fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.999998;stroke-opacity:1\" d=\"m 69.458088,66.110058 c -5.577131,-5.577105 -8.623667,-2.15373 -3.234982,3.23498 5.388687,5.388686 8.812037,2.342405 3.234982,-3.23498 z M 52.656671,52.82976 c 2.58848,-2.618008 11.989414,2.548852 19.761127,10.320564 7.771737,7.771711 12.938675,17.172719 10.320591,19.761148 C 80.120379,85.499926 69.965844,79.875417 62.829286,72.73886 55.692703,65.602277 50.068294,55.447845 52.656671,52.82976 Z\" id=\"path10129\"/><path style=\"fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.999998;stroke-opacity:1\" d=\"m 28.882279,61.094692 5.324707,8.190498 c -3.612484,6.5433 -7.488153,21.849445 1.369197,30.706795 8.85735,8.857345 24.163471,4.981675 30.706773,1.369165 l 8.190498,5.32473 C 59.697248,115.17451 41.341376,116.90065 30.004457,105.5637 18.667537,94.22679 20.393657,75.870871 28.882279,61.094692 Z\" id=\"path10127\"/><path style=\"fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.999998;stroke-opacity:1\" d=\"m 105.41317,105.58627 c 6.43005,-6.690404 0.38331,-19.274654 -2.30089,-25.163369 C 112.7565,65.935052 119.62899,44.245543 105.4757,30.092253 91.322429,15.938975 69.63289,22.811441 55.145088,32.455695 49.256373,29.771489 36.672096,23.72475 29.981737,30.154804 c -6.690361,6.43008 0.133365,28.72858 23.41803,52.013394 23.284817,23.284792 45.583347,30.108432 52.013403,23.418072 z M 36.072591,36.245657 c 2.067617,-2.067616 8.673802,0.176205 11.329039,1.657169 -1.940549,2.144321 -3.293478,2.969297 -4.610655,4.553892 0.873068,3.237684 2.215797,5.925052 3.860293,8.258863 2.791383,-2.674338 5.785567,-5.570296 8.577002,-8.244609 7.840742,5.433668 14.112773,9.971316 21.005169,16.863711 6.892422,6.892394 11.430071,13.164425 16.863713,21.005165 -2.674312,2.791459 -5.570246,5.785643 -8.244584,8.577026 2.333785,1.644496 5.021181,2.987198 8.258838,3.860292 1.584623,-1.317202 2.409598,-2.670105 4.553919,-4.610654 1.480964,2.655264 3.724755,9.261421 1.657144,11.329037 C 97.254852,101.56316 81.994298,99.432309 59.06495,76.503195 36.135601,53.57385 34.004975,38.313274 36.072591,36.245657 Z m 26.857725,1.195568 c 11.643225,-7.990781 28.785246,-10.077192 37.029524,-1.832914 8.24428,8.244277 6.15787,25.386294 -1.832915,37.029517 C 92.901414,65.53182 87.39882,59.381271 81.792848,53.7753 76.186876,48.16933 70.036326,42.666736 62.930316,37.441225 Z\" id=\"path10237\"/><path style=\"fill:#000000;fill-opacity:1;stroke:none;stroke-width:0.999998;stroke-opacity:1\" d=\"m 18.118242,122.75736 4.651838,-4.65181 c 22.682942,22.68292 66.195766,23.76325 92.5607,-2.60168 26.36492,-26.364916 25.81466,-68.906718 2.38117,-92.340197 l 5.08394,-5.08393 c 6.13834,0 8.05872,-5.616213 4.13296,-9.5419458 -3.92572,-3.9257249 -9.54193,-2.0053863 -9.54193,4.1329838 l -5.08393,5.083929 C 88.869507,-5.6787659 46.327724,-6.229038 19.962809,20.135879 -6.4021285,46.500788 -5.3218209,90.013629 17.361121,112.69662 l -4.651863,4.65181 c -6.6706998,0.47867 -8.1921321,7.36259 -5.0728696,10.48186 3.1192616,3.11925 10.0034216,1.59777 10.4818536,-5.07293 z M 27.417099,27.590167 C 47.974718,7.0325578 84.584869,3.6705843 108.19047,27.276161 c 23.60559,23.605578 20.24362,60.215747 -0.314,80.773379 -20.557619,20.5576 -56.995568,24.29435 -80.874664,0.41547 C 3.1227093,84.585878 6.8594798,48.14796 27.417099,27.590346 Z\" id=\"path10243\"/></g><g aria-label=\"GYR\" transform=\"matrix(4.6139872,0,0,4.6139872,4993.2602,2071.5273)\" id=\"text2014\" style=\"font-size:40px;line-height:1.25;letter-spacing:0px;word-spacing:0px;white-space:pre;shape-inside:url(#rect2016);stroke-width:0.0573439\"><path d=\"m -1053.0854,-433.81623 h -11.0215 v 1.60797 h 9.2565 q -0.3922,3.765 -3.1378,6.74563 -3.6084,3.96109 -9.531,3.96109 -5.5305,0 -9.2174,-3.60813 -3.6085,-3.60812 -3.6085,-9.17718 0,-5.52985 3.6477,-9.17719 3.6477,-3.64734 9.1782,-3.64734 6.7855,0 10.5509,5.17687 l 1.2943,-0.98047 q -4.3145,-5.76515 -11.8452,-5.76515 -6.2757,0 -10.3549,4.07875 -4.0399,4.03953 -4.0399,10.31453 0,6.35343 4.0399,10.39297 4.0008,4.00031 10.3549,4.00031 6.6678,0 10.7077,-4.27485 3.7261,-3.92187 3.7261,-9.64781 z\" style=\"font-family:GeosansLight;-inkscape-font-specification:'GeosansLight, Normal';stroke:#000000;stroke-width:0.573438;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\" id=\"path11121\"/><path d=\"m -1037.5925,-448.28795 h -1.8434 l -6.9816,12.1186 -6.9033,-12.1186 h -1.8434 l 7.9622,14.0011 v 14.00109 h 1.6081 v -14.00109 z\" style=\"font-family:GeosansLight;-inkscape-font-specification:'GeosansLight, Normal';stroke:#000000;stroke-width:0.573438;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\" id=\"path11123\"/><path d=\"m -1019.9031,-420.28576 -8.982,-14.00109 q 1.6865,0 3.0986,-0.39219 1.412,-0.43141 2.4317,-1.29422 1.059,-0.86281 1.6082,-2.19625 0.5883,-1.33344 0.5883,-3.17672 0,-1.6864 -0.6275,-2.98062 -0.5883,-1.29422 -1.6082,-2.15703 -1.0198,-0.90203 -2.4317,-1.33344 -1.412,-0.47063 -2.981,-0.47063 h -6.4717 v 28.00219 h 1.5689 v -14.00109 h 3.0201 l 8.9036,14.00109 z m -2.9025,-21.09969 q 0,2.66688 -1.5689,4.07875 -1.5297,1.41188 -4.0399,1.41188 h -5.2951 v -10.78516 h 4.8636 q 1.2551,0 2.3534,0.35297 1.0982,0.31375 1.9218,0.98048 0.8237,0.6275 1.2944,1.64718 0.4707,0.98047 0.4707,2.3139 z\" style=\"font-family:GeosansLight;-inkscape-font-specification:'GeosansLight, Normal';stroke:#000000;stroke-width:0.573438;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1\" id=\"path11125\"/></g></svg>"
                "</div>"

                "<h1>Control</h1>"

                 "<div>"
                    "<div>"
                       "<p id=\"active-button\">"
                           "<input type=\"hidden\" id=\"active\" name=\"active\" value=\"";
    ptr += ( active ? 0 : 1 );
    ptr += "\">"
                            "<button id=\"active_button\" style=\"background-color:";
    ptr += activeColor;
    ptr += ";\" onclick=\"ajax('active')\" class=\"button mdi mdi-power\">"
                              "<svg width=\"40\" height=\"40\" viewBox=\"0 0 24 24\"><path d=\"M16.56,5.44L15.11,6.89C16.84,7.94 18,9.83 18,12A6,6 0 0,1 12,18A6,6 0 0,1 6,12C6,9.83 7.16,7.94 8.88,6.88L7.44,5.44C5.36,6.88 4,9.28 4,12A8,8 0 0,0 12,20A8,8 0 0,0 20,12C20,9.28 18.64,6.88 16.56,5.44M13,3H11V13H13\" /></svg>"
                            "</button>"
                        "</p>"
                    "</div>"
                "</div>"

                "<div class=\"divTable\" style=\"width: 100%;\">"
                    "<div class=\"divTableBody\">"
                        "<div class=\"divTableRow\">"
                            "<div class=\"divTableCell\">"
                                "<div>"
                                    "<p>"
                                        "<span>"
                                          "<svg class=\"gauge\" width=\"45\" height=\"45\" viewBox=\"0 0 24 24\"><path d=\"M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,4A8,8 0 0,1 20,12C20,14.4 19,16.5 17.3,18C15.9,16.7 14,16 12,16C10,16 8.2,16.7 6.7,18C5,16.5 4,14.4 4,12A8,8 0 0,1 12,4M14,5.89C13.62,5.9 13.26,6.15 13.1,6.54L11.81,9.77L11.71,10C11,10.13 10.41,10.6 10.14,11.26C9.73,12.29 10.23,13.45 11.26,13.86C12.29,14.27 13.45,13.77 13.86,12.74C14.12,12.08 14,11.32 13.57,10.76L13.67,10.5L14.96,7.29L14.97,7.26C15.17,6.75 14.92,6.17 14.41,5.96C14.28,5.91 14.15,5.89 14,5.89M10,6A1,1 0 0,0 9,7A1,1 0 0,0 10,8A1,1 0 0,0 11,7A1,1 0 0,0 10,6M7,9A1,1 0 0,0 6,10A1,1 0 0,0 7,11A1,1 0 0,0 8,10A1,1 0 0,0 7,9M17,9A1,1 0 0,0 16,10A1,1 0 0,0 17,11A1,1 0 0,0 18,10A1,1 0 0,0 17,9Z\" /></svg>"
                                        "</span> <strong id=\"rpm_value\">";
    ptr += rps;
    ptr += "</strong> <strong>RPS</strong>"
                                    "</p>"
                                "</div>"
                                "<div>"
                                    "<p>"
                                        "<input id=\"rpm\" name=\"rpm\" type=\"range\" step=\"0.1\" min=\"0\" max=\"25\" value=\"";
    ptr += rps;
    ptr += "\" class=\"slider\" onchange=\"ajax('rpm')\" oninput=\"showValue('rpm')\" />"
                                    "</p>"
                                "</div>"
                            "</div>"
                        "</div><div class=\"divTableRow\">"
                            "<div class=\"divTableCell\">"
                                "<div>"
                                    "<p>"
                                        "<span>"
                                          "<svg class=\"gauge\" width=\"45\" height=\"45\" viewBox=\"0 0 24 24\" viewBox=\"0 0 24 24\"><path d=\"M20,19H4.09L14.18,4.43L15.82,5.57L11.28,12.13C12.89,12.96 14,14.62 14,16.54C14,16.7 14,16.85 13.97,17H20V19M7.91,17H11.96C12,16.85 12,16.7 12,16.54C12,15.28 11.24,14.22 10.14,13.78L7.91,17Z\" /></svg>"
                                        "</span> <strong id=\"angle_value\">";
    ptr += "0";
    ptr += "</strong><strong>&deg;</strong>"
                                    "</p>"
                                "</div>"
                                "<div>"
                                    "<p style=\"height:30px;\">"
                                        "<input id=\"angle\" name=\"angle\" step=\"0.5\" type=\"range\" min=\"1\" max=\"360\" value=\"";
    ptr += "0";
    ptr += "\" class=\"slider\" oninput=\"showValue('angle')\" style=\"width:240px;margin-right:20px;\" />"
                                        "<button id=\"angle-button\" onclick=\"ajax('angle')\" class=\"button\">"
                                          "<svg width=\"24\" height=\"21\" viewBox=\"0 0 24 21\"><path d=\"M2,21L23,12L2,3V10L17,12L2,14V21Z\" /></svg>"
                                        "</button>"
                                    "</p>"
                                "</div>"
                            "</div>"
                        "</div>"
                    "</div>"
                "</div>"
            "</body>"
        "</html>\n";

    return ptr;
}
