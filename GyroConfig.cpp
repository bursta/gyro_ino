#include "Arduino.h"
#include "GyroConfig.h"


GyroConfig::GyroConfig( 
    float _rps,
    float _angle,
    bool _active,
    ColorMatrix _colorMatrix )
{
    setRPS( _rps );
    setAngle( _angle );
    setActive( _active );
    setMatrix( _colorMatrix );
}


void GyroConfig::setRPS( float _rps )
{
    rps = _rps;

    velocity = rps / 1000000.0;
    velocityDegree = velocity * 360.0;
}


float GyroConfig::getRPS()
{
    return rps;
}

void GyroConfig::addAngle( float _angle )
{
    angle += _angle;
}

void GyroConfig::setAngle( float _angle )
{
    angle = _angle;
}


float GyroConfig::getAngle()
{
    return angle;
}


void GyroConfig::setAngleByDelay( unsigned long _scanGap_us )
{
    angle += (velocityDegree * _scanGap_us);
}


void GyroConfig::setActive( bool _active )
{
    active = _active;
}


bool GyroConfig::getActive()
{
     return active;
}


void GyroConfig::setMatrix( ColorMatrix _matrix )
{
    colorMatrix = _matrix;
}


ColorMatrix GyroConfig::getMatrix()
{
    return colorMatrix;
}
