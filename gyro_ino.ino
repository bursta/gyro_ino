#define LED_COUNT 2
#define SCANGAP_US 100

#define DATA_PIN 13
#define CLOCK_PIN 14

#define START_RPS 2.0
#define START_ANGLE 0.0
#define START_ACTIVE true

#define DEV true


//#include <SPI.h>
#include <NeoPixelBus.h>
#include "GyroConfig.h"
#include "GyroControl.h"
#include "GyroPixel.h"

xSemaphoreHandle semaphore = NULL;
TaskHandle_t commit_task;

// network access data
const char* NETWORK_SSID = "gyro";
const char* PASSWORD = "1234567899";

const float GEAR_RATIO = (100.0/ 29.0) * (60.0 / 20.0);


// for now use prerendered static color matrix
const ColorMatrix COLOR_MATRIX = {
    {
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), RgbColor(255, 0, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0), 
        RgbColor(0, 255, 0), RgbColor(0, 255, 0), RgbColor(0, 255, 0)
    },
    { 0, 3, 7, 11, 15, 21, 28, 37, 49, 67, 103, 121, 133, 142, 149, 155, 159, 163, 167, 170 }
};


GyroConfig settings( 
    START_RPS,
    START_ANGLE,
    START_ACTIVE,
    COLOR_MATRIX );

GyroControl gyroControl( 
    NETWORK_SSID,
    PASSWORD );

GyroPixel gyroPixel( 
    CLOCK_PIN, 
    DATA_PIN, 
    settings.getMatrix(),
    LED_COUNT, GEAR_RATIO );

RgbColor colors[LED_COUNT];



void commitTaskProcedure(void *arg)
{
    while (true)
    {
        while (ulTaskNotifyTake(pdTRUE, portMAX_DELAY) != 1)
            ;
          gyroPixel.setColors( colors, settings, SCANGAP_US );
        while (!gyroPixel.canShow())
            ;
        xSemaphoreGive(semaphore);
    }
}

void commit()
{
    xTaskNotifyGive(commit_task);
    while (xSemaphoreTake(semaphore, portMAX_DELAY) != pdTRUE)
        ;
}

void init_task()
{
    commit_task = NULL;
    semaphore = xSemaphoreCreateBinary();

    xTaskCreatePinnedToCore(
        commitTaskProcedure,         /* Task function. */
        "ShowRunnerTask",            /* name of task. */
        10000,                       /* Stack size of task */
        NULL,                        /* parameter of the task */
        4,                           /* priority of the task */
        &commit_task,                /* Task handle to keep track of created task */
        1);                          /* pin task to core core_id */
}

// INIT system
void setup() {
    Serial.begin( 115200 );
    
    gyroControl.init();
    if (DEV) Serial.println( "Web Control started" );

    gyroPixel.reset();
    if (DEV) Serial.println( "Pixel reseted" );

    settings.setRPS(START_RPS);
    if (DEV) Serial.print( "RPS set to " );
    if (DEV) Serial.println( START_RPS );

    init_task();
}


// PROCESS system
void loop() {
    gyroControl.onConnect( settings );

    if ( settings.getActive() )
    {
        //gyroPixel.setColors( colors, settings, SCANGAP_US );
        commit();
        delay(10);
    } else {
        //gyroPixel.reset();
        delay(100);
    }
}
