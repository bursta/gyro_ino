#ifndef GyroPixel_h
#define GyroPixel_h

#include "Arduino.h"
#include <NeoPixelBus.h>
#include "GyroConfig.h"


// PIXEL
/*
 *  calculates the current pixel color of the spinning LEDs
 */


class GyroPixel {
    public:
        GyroPixel( 
            uint8_t _clockPin,
            uint8_t _dataPin, 
            ColorMatrix _colorMatrix, 
            uint8_t _ledCount, 
            float _gearRatio );
        void reset();
        void getColors( 
            RgbColor* colors, 
            float _angle );
        void setColors( 
            RgbColor* colors, 
            GyroConfig& settings, 
            unsigned long _scanGap_us );
        bool canShow( );
        
    private:
        ColorMatrix colorMatrix;
        uint8_t rowCount;
        float gearRatio;
        uint8_t ledCount;
        float ledAngle;
        float segmentAngle;
        unsigned long lastTick_us;
        NeoPixelBus<DotStarBgrFeature, DotStarHspiMethod> pixels;
};

#endif
