#ifndef GyroConfig_h
#define GyroConfig_h

#include "Arduino.h"
#include <NeoPixelBus.h>

// CONFIG
/*
 *  Hold s all needed data for the projected image, like velocity,
 *  angle and color matrix
 *  
 * MATRIX structure
 *  array of row begin
 *  1. row 0 (5 colors)
 *  2. row 5      {0, 5, ...}
 *  rowStarts: index is column; value is index startpoint of row colors;
 *  next value -1 is end of row
 */

typedef struct
{
    RgbColor colors[170];
    uint16_t rowStarts[20];
}  ColorMatrix;


class GyroConfig
{
    public:
        GyroConfig( 
            float _rps, 
            float _angle, 
            bool _active, 
            ColorMatrix _colorMatrix );
        void setRPS( float _rps );
        float getRPS();
        void addAngle( float _angle );
        void setAngle( float _angle );
        float getAngle();
        void setAngleByDelay( unsigned long _scanGap_us );
        void setActive( bool _active );
        bool getActive();
        void setMatrix( ColorMatrix _matrix );
        ColorMatrix getMatrix();
        
    private:
        ColorMatrix colorMatrix;
        float velocity;
        float velocityDegree;
        float gearRatio;
        float rps;
        float angle;
        bool active;
};

#endif
