#ifndef GyroControl_h
#define GyroControl_h

#include "Arduino.h"

#include <WiFi.h>
#include "GyroConfig.h"


// CONTROL
/* 
 *  Simple Web-Interface to set options for velocity and angle of the Gyro
 *  You can also turn the flickering of the LEDs on and off
 *  It handles GET paramters, set specific options and send the HTML response
 */


class GyroControl
{
    public:
        GyroControl( 
            const char* wifi_ssid, 
            const char* wifi_password );
        void init();
        void onConnect( GyroConfig& settings );
        String sendHTML( 
            float rpm, 
            float angle, 
            bool active );
        
    private:
        WiFiServer server;
        const char* password;
        const char* ssid;
        IPAddress local_ip;
        IPAddress gateway;
        IPAddress subnet;
};

#endif