#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))

#include "Arduino.h"
#include "GyroPixel.h"


GyroPixel::GyroPixel( 
    uint8_t _clockPin,
    uint8_t _dataPin,
    ColorMatrix _colorMatrix,
    uint8_t _ledCount,
    float _gearRatio )
    : pixels(_ledCount)
{
    lastTick_us = micros();
    colorMatrix = _colorMatrix;
    ledCount = _ledCount;
    gearRatio = _gearRatio;

    ledAngle = 360 / ledCount;
    rowCount = NELEMS( colorMatrix.rowStarts ) - 2; // last entry is just for end of last row
    segmentAngle = 180.0 / (float)( rowCount );

    pixels.Begin( _clockPin, _dataPin, _dataPin, -1 );
}


void GyroPixel::reset() 
{
    RgbColor white( 255 );
    pixels.ClearTo( white );
    pixels.Show();
}


void GyroPixel::getColors( 
    RgbColor* colors,
    float _angle )
{
    int colorCount = 0;

    // calculate angle of inner layer
    float angleYZ = fmod( ( _angle * gearRatio ), 360.0 );

    // hold info about the darkside of the moon ;)
    bool backSide = false;
    if (angleYZ < 180)
    {
        backSide = true;
    }

    // calculate which row the led is in (Layer YZ)
    uint16_t angleRow = round( 
        (float )( 
            backSide ? angleYZ : (angleYZ - 180)
        ) / segmentAngle );

    //calulate which column the led is in (Layer XZ)
    uint16_t colCount = colorMatrix.rowStarts[angleRow + 1] 
        - colorMatrix.rowStarts[angleRow];

    // get index of color array by specific start index + column count
    uint16_t colorIndex = colorMatrix.rowStarts[angleRow] 
        + round( 
            ( 
                fmod( _angle + ( backSide ? 180 : 0 ), 360.0 ) 
                / 360.0 
            ) 
            * (float) ( colCount - 1 )
        );

    // set leds color array
    colors[colorCount++] = colorMatrix.colors[colorIndex];

    // repeat procedure above for any further leds 
    for ( int i=1; i<ledCount; i++ )
    {
        angleYZ = fmod( 
            (
                angleYZ 
                + ledAngle 
            ), 360.0 );

        bool backSide = false;
        if ( angleYZ < 180 )
        {
            backSide = true;
        }

        uint16_t angleRow = round( 
                (float) ( backSide ? angleYZ : ( angleYZ - 180 ) ) 
                / segmentAngle 
            );

        uint16_t colCount = colorMatrix.rowStarts[angleRow + 1] 
            - colorMatrix.rowStarts[angleRow];

        uint16_t colorIndex = colorMatrix.rowStarts[angleRow] 
            + round( 
                ( 
                    fmod( _angle + ( backSide ? 180 : 0 ), 360.0 ) 
                    / 360.0
                )
                * (float) ( colCount - 1 )
            );

        colors[colorCount++] = colorMatrix.colors[colorIndex];
    }
}


bool GyroPixel::canShow() {
  return pixels.CanShow();
}


void GyroPixel::setColors( 
    RgbColor* colors,
    GyroConfig& settings,
    unsigned long _scanGap_us )
{
    unsigned long currentTick_us = micros();
    unsigned long currentScanGap_us = currentTick_us - lastTick_us;

    if ( currentScanGap_us >= _scanGap_us )
    {
        settings.setAngleByDelay( currentScanGap_us );

        float currentAngle = settings.getAngle();

        getColors( colors, currentAngle );

        for ( int i=0; i<ledCount; i++ )
        {
            pixels.SetPixelColor( i, colors[i] );    
        }

        pixels.Show();

        lastTick_us = currentTick_us;
    }
}
